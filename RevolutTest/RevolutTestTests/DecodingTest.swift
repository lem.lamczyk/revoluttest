//
//  DecodingTest.swift
//  RevolutTestTests
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import XCTest
@testable import RevolutTest

fileprivate struct ConversionProperties {
    static let base = "EUR"
    static let ratesCount = 32
    static let currencyBase = "AUD"
    static let currencyValue = 1.6223
}

class DecodingTest: XCTestCase {

    func testShouldDecodeConversionListProperly() {
        guard let json = JSONHelper.json(withName: "ConversionList") else {
            assertionFailure("JSON retrieved from file cannot be null")
            return
        }
        let conversion = try? JSONDecoder().decode(Conversion.self, from: json)
        XCTAssertNotNil(conversion)
        XCTAssertEqual(conversion!.base, ConversionProperties.base)
        XCTAssertEqual(conversion!.rates?.count, ConversionProperties.ratesCount)
        conversion!.rates?.forEach {
            if $0.base == ConversionProperties.currencyBase {
                XCTAssertEqual($0.value, ConversionProperties.currencyValue)
            }
        }
    }
    
    func testShouldProperlyDecodeWithEmptyRates() {
        guard let json = JSONHelper.json(withName: "ConversionListEmptyRates") else {
            assertionFailure("JSON retrieved from file cannot be null")
            return
        }
        let conversion = try? JSONDecoder().decode(Conversion.self, from: json)
        XCTAssertNotNil(conversion)
        XCTAssertEqual(conversion!.base, ConversionProperties.base)
        XCTAssertEqual(conversion!.rates?.count, 0)
    }
    
    func testShouldProperlyDecodeWithNoRates() {
        guard let json = JSONHelper.json(withName: "ConversionListEmpty") else {
            assertionFailure("JSON retrieved from file cannot be null")
            return
        }
        let conversion = try? JSONDecoder().decode(Conversion.self, from: json)
        XCTAssertNotNil(conversion)
        XCTAssertEqual(conversion!.base, ConversionProperties.base)
        XCTAssertNil(conversion!.rates)
    }
}

fileprivate class JSONHelper {
    static func json(withName name: String) -> Data? {
        let bundle = Bundle(for: DecodingTest.self)
        guard let jsonURL = bundle.url(forResource: name, withExtension: "json") else {
            return nil
        }
        return try? Data(contentsOf: jsonURL)
    }
}
