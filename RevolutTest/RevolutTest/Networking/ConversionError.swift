//
//  ConversionError.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 23/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

enum ConversionError: Error {
    case unknown
    case json
    case server(String)
    
    func errorDescription() -> String {
        switch self {
        case .unknown:
            return "Unknown error ocurred"
        case .json:
            return "Data is corrupted"
        case .server(let description):
            return description
        }
    }
}
