//
//  ConversionWebService.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

enum ConversionServiceResult {
    case success(Conversion)
    case failure(ConversionError)
}

class ConversionWebService: WebService<Conversion>, ConversionProvider {
    
    func conversions(forBase base: String,
                     completion: @escaping (ConversionServiceResult) -> Void) {
        guard let url = URL(string: Defaults.endpoint + base) else {
            return
        }
        get(url: url) { [weak self] (result) in
            switch result {
            case .failure(let error):
                self?.handle(error: error, completion: completion)
            case .success(let result):
                self?.handle(result: result, completion: completion)
            }
        }
    }
    
    private func handle(error: Error?, completion: @escaping (ConversionServiceResult) -> Void) {
        guard let error = error else {
            completion(.failure(.unknown))
            return
        }
        if let _ = error as? DecodingError {
            completion(.failure(.json))
            return
        }
        let nserror = error as NSError
        completion(.failure(.server(nserror.localizedDescription)))
    }
    
    private func handle(result: Conversion,
                        completion: @escaping (ConversionServiceResult) -> Void) {
        completion(.success(result))
    }
}
