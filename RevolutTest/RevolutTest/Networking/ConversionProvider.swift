//
//  ConversionProvider.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

protocol ConversionProvider {
    func conversions(forBase base: String, completion: @escaping (ConversionServiceResult) -> Void)
}
