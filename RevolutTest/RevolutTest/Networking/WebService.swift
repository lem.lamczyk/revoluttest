//
//  WebService.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error?)
}

enum WebError: Error {
    case apiError(Data)
}

class WebService<R: Decodable> {

    func get(url: URL, completion: @escaping (Result<R>) -> Void) {
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, response, error) in
            guard let result = try? self.result(for: data, response, error) else {
                return
            }
            DispatchQueue.main.async  {
                completion(result)
            }
        }
        task.resume()
    }
    
    private func result(for data: Data?, _ response: URLResponse?, _ error: Error?) throws -> Result<R> {
        guard let data = data else {
            return .failure(error)
        }
        guard let response = response as? HTTPURLResponse else {
            return .failure(error)
        }
        guard response.statusCode < 400 else {
            return .failure(WebError.apiError(data))
        }
        do {
            let result = try JSONDecoder().decode(R.self, from: data)
            return .success(result)
        } catch let parsingError {
            return .failure(parsingError)
        }
    }
}
