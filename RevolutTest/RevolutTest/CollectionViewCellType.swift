//
//  CollectionViewCellType.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import UIKit

struct CollectionViewCellType {
    let reuseIdentifier: String
    let nibFile: String
    
    /// Uses the provided identifier as both reuse identifier and nib file name.
    init(identifier: String) {
        self.reuseIdentifier = identifier
        self.nibFile = identifier
    }
    
    static let conversionCell = CollectionViewCellType(identifier: "ConversionCell")
}
