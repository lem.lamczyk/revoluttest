//
//  ErrorHandler.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 24/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import UIKit

class ErrorHandler {
    func alert(for error: ConversionError, completion: @escaping () -> () = {}) -> UIAlertController {
        let alert = UIAlertController(title: "Error ocurred", message: error.errorDescription(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            completion()
        }))
        return alert
    }
}
