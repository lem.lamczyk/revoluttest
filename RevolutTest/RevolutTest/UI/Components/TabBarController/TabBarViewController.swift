//
//  TabBarViewController.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import UIKit

class TabBarViewController: UIViewController {

    @IBOutlet private weak var container: UIStackView!
    @IBOutlet weak var tabBar: TabBar!
    
    struct Tab {
        let title: String
        let viewController: UIViewController
    }
    
    public var tabs: [Tab] = [] {
        didSet {
            tabViewControllers = tabs.map { $0.viewController }
            updateUI()
        }
    }
    
    public var selectedTabIndex: Int? {
        get { return tabBar.selectedTab }
        set {
            let oldValue = selectedTabIndex
            guard oldValue != newValue else { return }
            tabBar.selectedTab = newValue
            updateTabContent(toTabIndex: newValue, from: oldValue)
        }
    }
    
    private var tabViewControllers: [UIViewController] = []
    
    private var tabContentController: UIPageViewController!
    
    public var selectedViewController: UIViewController? {
        return selectedTabIndex.map { tabViewControllers[$0] }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.delegate = self
        setupTabContentController()
        updateUI()
    }
    
    private func setupTabContentController() {
        tabContentController = UIPageViewController(transitionStyle: .scroll,
                                                    navigationOrientation: .horizontal,
                                                    options: nil)
        addChild(tabContentController)
        tabContentController.view.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(tabContentController.view, belowSubview: container)
        NSLayoutConstraint.activate([
            tabContentController.view.leftAnchor.constraint(equalTo: view.leftAnchor),
            tabContentController.view.rightAnchor.constraint(equalTo: view.rightAnchor),
            tabContentController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,
                                                           constant: tabBar.frame.height),
            tabContentController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
        tabContentController.didMove(toParent: self)
        tabContentController.dataSource = self
        tabContentController.delegate = self
    }
    
    private func updateUI() {
        guard isViewLoaded else { return }
        tabBar.tabs = tabs.map { $0.title }
    }
    
    private func tabIndex(for viewController: UIViewController) -> Int? {
        return tabViewControllers.index(of: viewController)
    }
    
    private func updateTabContent(toTabIndex newTabIndex: Int?, from oldTabIndex: Int?) {
        guard isViewLoaded else { return }
        tabContentController.setViewControllers([selectedViewController ?? UIViewController()],
                                                direction: .from(oldTabIndex, to: newTabIndex),
                                                animated: oldTabIndex != nil) { _ in }
    }
}

// MARK: - UIPageViewControllerDelegate

extension TabBarViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating _: Bool,
                            previousViewControllers _: [UIViewController],
                            transitionCompleted _: Bool) {
        guard let newViewController = pageViewController.viewControllers?.first else {
            return
        }
        let index = tabIndex(for: newViewController)
        tabBar.selectedTab = index
    }
}

// MARK: - UIPageViewControllerDataSource

extension TabBarViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController)
        -> UIViewController? {
            guard let index = tabIndex(for: viewController) else { return nil }
            
            let nextIndex = index + 1
            guard nextIndex < tabs.count else { return nil }
            return tabViewControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController)
        -> UIViewController? {
            guard let index = tabIndex(for: viewController) else { return nil }
            
            let nextIndex = index - 1
            guard nextIndex >= 0 && nextIndex < tabs.count else { return nil }
            return tabViewControllers[nextIndex]
    }
}

// MARK: - TabBarDelegate

extension TabBarViewController: TabBarDelegate {
    func tabbar(_ tabbar: TabBar, didChangeTabFrom oldTab: Int?, to newTab: Int?) {
        guard isViewLoaded else { return }
        tabContentController.setViewControllers([selectedViewController ?? UIViewController()],
                                                direction: .from(oldTab, to: newTab),
                                                animated: oldTab != nil) { _ in }
    }
}

private extension UIPageViewController.NavigationDirection {
    static func from(_ oldIndex: Int?, to newIndex: Int?)
        -> UIPageViewController.NavigationDirection {
            guard let oldIndex = oldIndex, let newIndex = newIndex else { return .forward }
            return newIndex >= oldIndex ? .forward : .reverse
    }
}
