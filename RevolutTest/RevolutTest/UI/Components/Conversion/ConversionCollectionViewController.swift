//
//  ConversionCollectionViewController.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import UIKit

class ConversionCollectionViewController: CollectionViewController {
    
    private let cellHeight: CGFloat = 80.0
    private let timeInterval = 1.0
    
    private let loadingIndicator = UIActivityIndicatorView(style: .gray)
    
    private let model = ConversionModel(provider: ConversionWebService())
    
    private let errorHandler = ErrorHandler()

    private var timer: Timer? {
        didSet {
            oldValue?.invalidate()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(cell: .conversionCell)
        setupLoadingIndicator()
        setupConversionProvider()
    }
    
    private func setupLoadingIndicator() {
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loadingIndicator)
        NSLayoutConstraint.activate([
            loadingIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loadingIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ])
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()
    }
    
    private func setupConversionProvider() {
        scheduleTimer()
        updateConversion()
        handleConversionChange()
    }
    
    private func handleConversionChange() {
        model.changeHandler = { [weak self] error in
            guard let error = error else {
                self?.reloadDataIfNeeded()
                self?.loadingIndicator.stopAnimating()
                return
            }
            self?.handleError(error)
        }
    }
    
    private func handleError(_ error: ConversionError) {
        resetTimer()
        let alert = errorHandler.alert(for: error) {
            self.scheduleTimer()
        }
        present(alert, animated: true)
    }
    
    private func reloadDataIfNeeded() {
        if collectionView.numberOfItems(inSection: 0) <= 1 {
            collectionView.reloadData()
        }
    }
    
    private func updateConversion() {
        model.getCurrentConversion()
    }
    
    private func updateRateOrderForCell(at indexPath: IndexPath) {
        guard let base = conversionCell(at: indexPath).rate?.base else { return }
        model.rateManager.moveToFront(base)
    }
    
    private func updateTextFieldInteractionForCell(at indexPath: IndexPath) {
        conversionCell(at: indexPath).disableTextField()
        //first cell always has enabled keyboard
        conversionCell(at: IndexPath(row: 0, section: 0)).enableTextField()
    }
    
    private func configure(_ cell: ConversionCell, at index: Int) {
        guard index >= 0, index < model.rateManager.rates.count else { return }
        cell.rate = model.rateManager.rates[index]
        cell.handleInteraction = { self.model.rateManager.update(with: $0) }
        if index != 0 { cell.disableTextField() }
    }
    
    private func moveCellToTop(_ indexPath: IndexPath) {
        resetTimer() //invalidate the timer for the time the cells are being swaped
        let startIndex = IndexPath(row: 0, section: 0)
        swapCell(at: indexPath, withCellAt: startIndex)  { [weak self] in
            self?.scheduleTimer()
        }
    }

    /// Swaps the given cell with the first one and scrolls to top if necessary
    private func swapCell(at startIndex: IndexPath,
                          withCellAt endIndex: IndexPath,
                          completion: @escaping ()->() = {} ) {
        collectionView.performBatchUpdates({ [weak self] in
            self?.collectionView.moveItem(at: startIndex, to: endIndex)
            self?.collectionView.moveItem(at: endIndex, to: startIndex)
            self?.collectionView.scrollToItem(at: endIndex, at: .top, animated: true)
        }) { _ in
            completion()
        }
    }
    
    /// Returns the cell at the given indexPath
    private func conversionCell(at indexPath: IndexPath) -> ConversionCell {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ConversionCell else {
            fatalError()
        }
        return cell
    }
}

// MARK: - UICollectionViewController methods

extension ConversionCollectionViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.rateManager.rates.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(cell: .conversionCell, for: indexPath) as? ConversionCell else {
            fatalError()
        }
        configure(cell, at: indexPath.item)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.item != 0 else { return }
        updateRateOrderForCell(at: indexPath)
        moveCellToTop(indexPath)
        updateTextFieldInteractionForCell(at: indexPath)
    }
}

// MARK: - Timer related methods

private extension ConversionCollectionViewController {
    func resetTimer() {
        timer?.invalidate()
    }
    
    func scheduleTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: timeInterval,
                                     repeats: true) { [weak self] _ in
                                        self?.updateConversion()
        }
        if let timer = timer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ConversionCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: cellHeight)
    }
}
