//
//  ConversionCell.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import UIKit

class ConversionCell: UICollectionViewCell {
 
    @IBOutlet private weak var currencyType: UILabel!
    @IBOutlet private weak var currencyAmount: UITextField!
    
    var rate: RateInfo? {
        didSet {
            currentCurrencyAmount = rate?.currencyAmount.value ?? 0
            updateCurrencyTypeUI(rate?.base)
            setupCurrencyValueObserving()
        }
    }

    private var currentCurrencyAmount: Double = 0.0 {
        didSet {
            updateCurrencyAmountUI(currentCurrencyAmount)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        rate?.currencyAmount.unsubscribe(observer: self)
        rate = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTextField()
    }
    
    private func setupTextField() {
        currencyAmount.addTarget(self,
                         action: #selector(textFieldDidChange(_:)),
                         for: UIControl.Event.editingChanged)
    }
    
    private func setupCurrencyValueObserving() {
        rate?.currencyAmount.unsubscribe(observer: self) // just to make sure there are no more subscriptions
        rate?.currencyAmount.subscribe(observer: self) { self.currentCurrencyAmount = $0 }
    }
    
    private func updateCurrencyAmountUI(_ value: Double) {
        currencyAmount.text = String(format: "%.2f", value)
    }
    
    private func updateCurrencyTypeUI(_ base: String?) {
        currencyType.text = rate?.base ?? ""
    }
    
    private func number(from string: String) -> NSNumber? {
        let formatter = NumberFormatter()
        formatter.decimalSeparator = "."
        return formatter.number(from: string)
    }

    @objc private func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text, let multiplier = number(from: text) else {
            handleInteraction?(0)
            return
        }
        handleInteraction?(multiplier.doubleValue)
    }
    
    typealias InteractionHandler = (_ multiplies: Double) -> Void
    var handleInteraction: InteractionHandler?
}

extension ConversionCell {
    func enableTextField() {
        currencyAmount.isUserInteractionEnabled = true
        currencyAmount.becomeFirstResponder()
    }

    func disableTextField() {
        currencyAmount.isUserInteractionEnabled = false
    }
}
