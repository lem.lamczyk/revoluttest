//
//  TabBar.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import UIKit

protocol TabBarDelegate: class {
    func tabbar(_ tabbar: TabBar,
                didChangeTabFrom oldTab: Int?,
                to newTab: Int?)
}

class TabBar: UIView {
    
    weak var delegate: TabBarDelegate?
    
    fileprivate class TabBarButton: UIButton {
        var fontSize: CGFloat = 16
        override var isSelected: Bool {
            didSet {
                if isSelected {
                    titleLabel?.font = UIFont.boldSystemFont(ofSize: fontSize)
                } else {
                    titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
                }
            }
        }
    }

    private let tabViewContainer: UIStackView
    
    var tabs: [String] = [] {
        didSet {
            updateTabViews(with: tabs)
        }
    }
    
    private var tabViews: [UIButton] = []
    
    var selectedTab: Int? {
        didSet {
            guard oldValue != selectedTab else { return }
            setTab(atIndex: oldValue, isSelected: false)
            setTab(atIndex: selectedTab, isSelected: true)
            moveSelectionIndicator(from: oldValue, to: selectedTab)
        }
    }
    
    private func setTab(atIndex index: Int?, isSelected: Bool) {
        guard let tabIndex = index else { return }
        assert(tabIndex < tabViews.count, "Selection must be in bounds of managed tabs")
        tabViews[tabIndex].isSelected = isSelected
    }
    
    private var selectionIndicator: UIView!

    override init(frame: CGRect) {
        let bounds = CGRect(origin: .zero, size: frame.size)
        tabViewContainer = UIStackView(frame: bounds)
        super.init(frame: frame)
        initUI()
    }

    required init?(coder aDecoder: NSCoder) {
        let bounds = CGRect(origin: .zero, size: .zero)
        tabViewContainer = UIStackView(frame: bounds)
        super.init(coder: aDecoder)
        initUI()
    }

    private func initUI() {
        initSelectionIndicator()
        initTabViewContainer()
    }
    
    private func initSelectionIndicator() {
        if selectionIndicator == nil {
            selectionIndicator = UIView()
            addSubview(selectionIndicator)
        }
        selectionIndicator.backgroundColor = .gray
    }
    
    private func initTabViewContainer() {
        tabViewContainer.axis = .horizontal
        tabViewContainer.distribution = .fillEqually
        addSubview(tabViewContainer)
        tabViewContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tabViewContainer.topAnchor.constraint(equalTo: topAnchor),
            tabViewContainer.bottomAnchor.constraint(equalTo: bottomAnchor),
            tabViewContainer.leftAnchor.constraint(equalTo: leftAnchor),
            tabViewContainer.rightAnchor.constraint(equalTo: rightAnchor)
            ])
    }
    
    private func moveSelectionIndicator(from oldIndex: Int?, to newIndex: Int?) {
        guard let tabIndex = newIndex else { return }
        assert(tabIndex < tabViews.count, "Selection must be in bounds of managed tabs")
        tabViewContainer.layoutIfNeeded()
        let selectionIndicatorHeight: CGFloat = 2.0
        let frame = CGRect(x: tabViews[tabIndex].frame.origin.x,
                           y: tabViews[tabIndex].frame.size.height - selectionIndicatorHeight,
                           width: tabViews[tabIndex].frame.size.width,
                           height: selectionIndicatorHeight)
        let animations = {() -> Void in
            self.selectionIndicator.frame = frame
        }
        
        if oldIndex != nil {
            UIView.animate(withDuration: 0.2,
                           delay: 0.0,
                           options: .curveEaseIn,
                           animations: animations,
                           completion: { _ in })
        } else {
            animations()
        }
    }
    
    private func updateTabViews(with tabs: [String]) {
        assert(tabs.count >= 0, "Count of tabs must be positive")
        if tabViews.count > 0 {
            tabViews.removeAll()
            tabViewContainer.removeAllArrangedSubviews()
        }
        tabs.forEach { title in
            let tab = createTabButton(with: title)
            tabViews.append(tab)
            tabViewContainer.addArrangedSubview(tab)
        }
    }
    
    private func createTabButton(with title: String) -> TabBarButton {
        let button = TabBarButton(type: .custom)
        button.addTarget(self,
                         action: #selector(requestTab),
                         for: .touchUpInside)
        button.setTitle(title, for: .normal)
        button.setTitleColor(.gray, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 16.0)
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.titleLabel?.textAlignment = .center
        return button
    }
    
    @objc private func requestTab(sender: UIButton) {
        guard let tabIndex = tabViews.index(of: sender) else { return }
        let oldSelectedTab = selectedTab
        selectedTab = tabIndex
        guard oldSelectedTab != selectedTab else { return }
        delegate?.tabbar(self, didChangeTabFrom: oldSelectedTab, to: selectedTab)
    }
}
