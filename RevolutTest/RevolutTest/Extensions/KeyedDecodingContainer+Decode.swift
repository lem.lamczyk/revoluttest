//
//  KeyedDecodingContainer+Decode.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 23/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//
//credit to: https://gist.github.com/loudmouth/332e8d89d8de2c1eaf81875cfcd22e24

import Foundation

struct JSONCodingKeys: CodingKey {
    var stringValue: String
    
    init?(stringValue: String) {
        self.stringValue = stringValue
    }
    
    var intValue: Int?
    
    init?(intValue: Int) {
        self.init(stringValue: "\(intValue)")
        self.intValue = intValue
    }
}

extension KeyedDecodingContainer {
    
    func decode(_ type: Dictionary<String, Double>.Type, forKey key: K) throws -> Dictionary<String, Double>? {
        let container = try? self.nestedContainer(keyedBy: JSONCodingKeys.self, forKey: key)
        return try container?.decode(type)
    }
    
    func decode(_ type: Dictionary<String, Double>.Type) throws -> Dictionary<String, Double>? {
        var dictionary = Dictionary<String, Double>()
        
        for key in allKeys {
            if let doubleValue = try? decode(Double.self, forKey: key) {
                dictionary[key.stringValue] = doubleValue
            }
        }
        return dictionary
    }
}
