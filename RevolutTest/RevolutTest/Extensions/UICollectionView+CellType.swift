//
//  UICollectionView+CellType.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import UIKit

extension UICollectionView {

    func register(cell: CollectionViewCellType) {
        register(UINib(nibName: cell.nibFile, bundle: nil),
                 forCellWithReuseIdentifier: cell.reuseIdentifier)
    }
    
    func dequeue(cell: CollectionViewCellType,
                 for indexPath: IndexPath) -> UICollectionViewCell {
        return dequeueReusableCell(withReuseIdentifier: cell.reuseIdentifier,
                                   for: indexPath)
    }
}
