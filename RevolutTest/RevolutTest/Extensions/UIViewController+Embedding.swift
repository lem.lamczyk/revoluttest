//
//  UIViewController+Embedding.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import UIKit

extension UIViewController {

    /// Adds a view controller as a child and it's view as a subview of into
    public func embed(viewController: UIViewController, into view: UIView) {
        addChild(viewController)
        let embedView: UIView = viewController.view
        view.anchorSubview(embedView)
        viewController.didMove(toParent: self)
    }
}
