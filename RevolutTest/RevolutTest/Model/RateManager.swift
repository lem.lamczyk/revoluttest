//
//  RateManager.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 20/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

class RateManager {

    var rates: [RateInfo] = []
    
    private var currentMultiplier: Double
    
    private var currentCurrencyAmount: Double {
        get { return rates.first?.currencyAmount.value ?? 0.0 }
    }

    init(rate: RateInfo) {
        rates.append(rate)
        currentMultiplier = rate.currencyAmount.value
    }
    
    public func moveToFront(_ base: String) {
        if let index = rates.firstIndex(where: {$0.base == base }) {
            rates.swapAt(0, index)
        }
        currentMultiplier = currentCurrencyAmount
    }
    
    public func updateRates(for conversion: Conversion) {
        if rates.count == 1 {
            append(conversion.rates)
        } else {
            update(with: conversion.rates)
        }
    }
    
    public func update(with multiplier: Double) {
        currentMultiplier = multiplier
        rates.dropFirst().forEach { $0.currencyAmount.value = rate($0.currencyRate) }
    }
    
    private func rate(_ value: Double) -> Double {
        return value * currentMultiplier
    }
    
    private func append(_ newRates: [Rate]?) {
        guard let newRates = newRates else { return }
        rates.append(contentsOf: newRates.map {
            RateInfo(base: $0.base, currencyRate: $0.value, currencyAmount: rate($0.value))
        })
    }
    
    private func update(with newRates: [Rate]?) {
        guard let newRates = newRates else { return }
        for rateInfo in rates {
            for rate in newRates {
                if rateInfo.base == rate.base {
                    rateInfo.currencyAmount.value = self.rate(rate.value)
                    rateInfo.currencyRate = rate.value
                }
            }
        }
    }
}
