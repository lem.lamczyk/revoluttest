//
//  RateInfo.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 20/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

protocol ObservableProtocol {
    associatedtype T
    var value: T { get set }
    func subscribe(observer: AnyObject,
                   block: @escaping (_ newValue: T) -> ())
    func unsubscribe(observer: AnyObject)
}

public final class Observable<T>: ObservableProtocol {
    
    typealias ObserverBlock = (_ newValue: T) -> ()
    typealias ObserversEntry = (observer: AnyObject, block: ObserverBlock)
    private var observers: Array<ObserversEntry>
    
    init(_ value: T) {
        self.value = value
        observers = []
    }
    
    var value: T {
        didSet {
            observers.forEach { (entry: ObserversEntry) in
                let (_, block) = entry
                block(value)
            }
        }
    }
    
    func subscribe(observer: AnyObject, block: @escaping ObserverBlock) {
        let entry: ObserversEntry = (observer: observer, block: block)
        observers.append(entry)
    }
    
    func unsubscribe(observer: AnyObject) {
        let filtered = observers.filter { entry in
            let (owner, _) = entry
            return owner !== observer
        }
        observers = filtered
    }
}
