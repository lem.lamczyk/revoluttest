//
//  ConversionModel.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

class ConversionModel {
    
    var rateManager: RateManager
    var changeHandler: ((ConversionError?) -> Void)?
    
    private let provider: ConversionProvider
    
    private var currentBase: String? {
        get { return rateManager.rates.first?.base }
    }

    init(provider: ConversionProvider) {
        self.provider = provider
        rateManager = RateManager(rate: Defaults.defaultRate)
    }
    
    func getCurrentConversion() {
        guard let base = currentBase else { return }
        provider.conversions(forBase: base) { [weak self] result in
            guard let handler = self?.changeHandler else { return }
            switch result {
            case .failure(let error):
                handler(error)
            case .success(let conversion):
                self?.rateManager.updateRates(for: conversion)
                handler(nil)
            }
        }
    }
}
