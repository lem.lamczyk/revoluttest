//
//  Conversion.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

struct Conversion: Decodable {
    let base: String
    let rates: [Rate]?

    private enum CodingKeys: String, CodingKey {
        case base = "base"
        case rates = "rates"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        base = try container.decode(String.self, forKey: .base).uppercased()
        let dictionary: [String: Double]? = try? container.decode([String: Double].self, forKey: .rates)
        rates = dictionary?.map { Rate(base: $0.key, value: $0.value) }
    }
}
