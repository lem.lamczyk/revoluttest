//
//  RateInfo.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 20/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

class RateInfo {
    
    public let base: String
    public var currencyRate: Double
    public var currencyAmount: Observable<Double>
    
    init(base: String, currencyRate: Double, currencyAmount: Double) {
        self.base = base
        self.currencyRate = currencyRate
        self.currencyAmount = Observable<Double>(currencyAmount)
    }
}
