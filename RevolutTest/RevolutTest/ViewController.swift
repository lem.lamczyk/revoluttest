//
//  ViewController.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 19/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let tabController = TabBarViewController()
    private let allRatesController = UIViewController()
    private let conversionController = ConversionCollectionViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBarViewController()
    }
    
    private func setupTabBarViewController() {
        embed(viewController: tabController, into: view)
        tabController.tabs = [ TabBarViewController.Tab(title: "All Rates",
                                                        viewController: allRatesController),
                               TabBarViewController.Tab(title: "Converter",
                                                        viewController: conversionController),
        ]
        tabController.selectedTabIndex = 1
        tabController.tabBar.backgroundColor = .white
    }
}

