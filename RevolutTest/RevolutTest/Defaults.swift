//
//  Defaults.swift
//  RevolutTest
//
//  Created by Mateusz Lamczyk on 24/01/2019.
//  Copyright © 2019 Mateusz Lamczyk. All rights reserved.
//

import Foundation

struct Defaults {
    static let endpoint = "https://revolut.duckdns.org/latest?base="
    // starting value indicating the amount of money the user has
    static let defaultRate = RateInfo(base: "EUR", currencyRate: 0.0, currencyAmount: 10.0)
}
